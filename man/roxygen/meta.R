list(
  rd_family_title = list(augment        = "Other data augmentation functions:",
                         data           = "Other data objects:",
                         helpers        = "Other plot styling helpers",
                         path           = "Other path functions:",
                         plot           = "Other plotting functions and objects:",
                         preassembly    = "Other functions around \\R object pre-assembly:",
                         report_assets  = "Other functions to manage report's asset files:",
                         report_config  = "Other functions around report's configuration:",
                         report_content = "Other functions to handle report's content:",
                         report_renv    = "Other functions to manage report's renv setup:",
                         table          = "Other table functions and objects:",
                         transform      = "Other data transformation functions:")
)
