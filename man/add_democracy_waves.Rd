% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rdb.report.gen.R
\name{add_democracy_waves}
\alias{add_democracy_waves}
\title{Add waves of democracy to referendum data}
\usage{
add_democracy_waves(data)
}
\arguments{
\item{data}{RDB referendum data as returned by \code{\link[rdb:rfrnds]{rfrnds()}}. A data frame that at minimum contains the column \code{date}.}
}
\value{
A \link[tibble:tbl_df-class]{tibble}.
}
\description{
Augments \code{data} with an additional column \code{wave_of_democracy} indicating in which one of the six democratization/autocratization waves the referendum took
place. The classification is based on the \href{https://en.wikipedia.org/wiki/Waves_of_democracy}{\emph{waves of democracy} theory} popularized by Samuel P. Huntington
and further developed i.a. by \href{https://www.tandfonline.com/doi/full/10.1080/13510347.2019.1582029}{Lührmann and Lindberg (2019)}.
}
\examples{
rdb::rfrnds(quiet = TRUE) |>
  rdb.report::add_democracy_waves() |>
  dplyr::select(id, date, wave_of_democracy)
}
\seealso{
Other data augmentation functions:
\code{\link{add_era}()}
}
\concept{augment}
