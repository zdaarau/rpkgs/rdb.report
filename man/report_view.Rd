% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rdb.report.gen.R
\name{report_view}
\alias{report_view}
\title{View report}
\usage{
report_view(
  profile = quappo::default_profiles(),
  output_format = NULL,
  host = "127.0.0.1",
  port = 3456L,
  in_rstudio_viewer = interactive()
)
}
\arguments{
\item{profile}{\href{https://quarto.org/docs/projects/profiles.html}{Quarto project profile(s)} to use. Either
a character vector of profile names or \code{NULL} to use the default profile.}

\item{output_format}{Quarto output format to open. If \href{https://quarto.org/docs/reference/formats/html.html}{\code{"html"}}, a local HTTP server is launched
serving the projects's HTML output under \code{host} on the specified \code{port}.}

\item{host}{IPv4 address to serve the HTML output on. A character scalar. Only relevant for \code{format = "html"}.}

\item{port}{TCP port number to serve the HTML output on. An integer scalar. Note that on most Unix-like systems including Linux and macOS, port numbers
below 1024 require root privileges. If the specified \code{port} is already occupied, a randomly chosen port between 1025–49151 is used instead. Only relevant
for \code{format = "html"}.}

\item{in_rstudio_viewer}{Whether to open the output file in the RStudio viewer pane if possible, i.e. RStudio \link[rstudioapi:isAvailable]{is available}. If
\code{FALSE} or RStudio is unavailable, the output file is opened in the system's default application for \code{output_format} via \code{\link[xopen:xopen]{xopen::xopen()}} instead. Note
that only PDFs within the \R session temporary directory can be opened in RStudio's viewer pane, others are automatically passed to the system's default
PDF viewer.}
}
\value{
For \code{format = "html"}, a \code{\link[httpuv:WebServer]{httpuv::WebServer}} object. For \code{format = "pdf"}, the path to the PDF file. Invisibly in both cases.
}
\description{
Thin wrapper around \code{\link[quappo:view_output]{quappo::view_output()}} that opens the rendered report in the specified \code{output_format}.
}
\examples{
\dontrun{
# only works from inside the rdb.report repo
## start server
s <- rdb.report::report_view()

## stop server
s$stop()}
}
\seealso{
Other functions to handle report's content:
\code{\link{report_gen_part_regional}()},
\code{\link{report_render}()},
\code{\link{report_render_partly}()}
}
\concept{report_content}
