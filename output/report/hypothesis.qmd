## Hypothes.is integration

It's possible to directly annotate and comment this report via the integrated [Hypothes.is](https://en.wikipedia.org/wiki/Hypothes.is) annotation system. Simply
open the Hypothes.is sidebar by clicking the <kbd>\<</kbd> button at the far right of the page to access the full user interface.

Annotations (visible to others) and highlights (only visible to yourself) can be made directly by selecting any text on this website. See [this help
article](https://web.hypothes.is/help/whats-the-difference-between-an-annotation-and-a-highlight/) to learn more about the different types of annotations that
are supported.

Annotations are stored in the currently selected Hypothes.is group. By default, this is the world-readable group
[*Public*](https://hypothes.is/groups/__world__/public?q=report.rdb.vote). You can select any other group you're a member of, as well as *🔒 Only Me* to keep an
annotation private to yourself.[^hypothesis-1]

Annotations can be formatted via [Markdown syntax](https://web.hypothes.is/help/formatting-annotations-with-markdown/).

[^hypothesis-1]: We actively monitor the *Public* group for annotations concerning this report. Make sure to post annotations to that group if you want to
    provide feedback to us.
