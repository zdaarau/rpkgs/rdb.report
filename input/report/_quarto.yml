profile:
  group:
    - [web, print, print_2023, minimal]

project:
  execute-dir: file
  lib-dir: libs
  output-dir: ../../output/report
  pre-render: assets/pre-render.R
  post-render: assets/post-render.R
  resources:
    - static
  type: book

bibliography: assets/bibliography.json
csl: "assets/citation_style.csl"
fig-cap-location: top
# TODO: create reprex for our CSL plus this option and report issue
# reference-location: margin
tbl-cap-location: top
lang: en-US
# source: https://quarto.org/docs/authoring/create-citeable-articles.html#google-scholar
# TODO: submit issue about error when specified (for books? or when bib not provided via `citation:` key but sep file?)
# google-scholar: true

book:
  title: "The World of Referendums"
  subtitle: "2024 edition"
  description: "Yearly Referendum Database (RDB) report"
  author:
    - name: Juri Ezzaini
      orcid: 0009-0003-9842-3131
    - name: Jonas Wüthrich
      orcid: 0000-0002-4548-1739
    - name: Salim Brüggemann
      orcid: 0000-0002-5329-5987
    - name: Kymani Koelewijn
      orcid: 0009-0008-2214-9718
    - name: Gianluca Sorrentino
      orcid: 0009-0006-2482-003X
    - name: Robin Gut
      orcid: 0000-0002-7617-7748
    - name: Uwe Serdült
      orcid: 0000-0002-2383-3158
  citation-key: rdbreport2024
  citation-label: rdbreport2024
  date: 2024-12-12
  date-format: long
  page-footer:
    left: "This work is licensed under <a href='https://creativecommons.org/licenses/by-sa/4.0/' rel=noopener target=_blank><i class='bi bi-cc-circle'></i> Creative Commons Attribution-ShareAlike 4.0 International</a>"
    right: "Copyright © 2024 <a href='https://www.zdaarau.ch/' rel=noopener target=_blank>Centre for Democracy Studies Aarau (ZDA) at the <a href='https://www.uzh.ch/' rel=noopener target=_blank>University of Zurich</a>, Switzerland"
    border: false
    background: "#343a40"
  number: 2
  publisher: "Zentrum für Demokratie Aarau (ZDA)"
  publisher-place: "Aarau"
  collection-number: 30
  collection-title: "Studienberichte des Zentrums für Demokratie Aarau"
  # doi: "?"
  issn: "2813-7426"
  isbn: "978-3-906918-42-6"
  language: "en-US"
  license: "CC BY-SA"
  type: report
  downloads:
    - pdf
  open-graph: true
  output-file: "world_of_referendums_2024"
  repo-actions:
    - edit
  repo-branch: master
  repo-subdir: input/report
  repo-url: https://gitlab.com/zdaarau/rpkgs/rdb.report
  site-url: https://report.rdb.vote/
  comments:
    hypothesis:
      # default URL used by Quarto is https://hypothes.is/embed.js which returns a HTTP 302 redirect with content type `text/html`, hence breaking the
      # `X-Content-Type-Options = "nosniff"` CSP, thus we explicitly set the redirect target URL here
      client-url: https://cdn.hypothes.is/hypothesis # cf. https://github.com/quarto-dev/quarto-cli/commit/a2b6663ad987e01e0ae9f13206b8017857052ced
      # for supported Hypothes.is config options, see https://h.readthedocs.io/projects/client/en/latest/publishers/config.html
      openSidebar: false
      showHighlights: whenSidebarOpen
      theme: classic
  back-to-top-navigation: true
  page-navigation: true
  reader-mode: false
  search:
    copy-button: true
    location: sidebar
    type: textbox
  twitter-card:
    site: "zdaarau"
crossref:
  # appendix-title: "Appendix"
  # appendix-delim: ":"
  lof-title: "List of figures"
  lot-title: "List of tables"

format:
  html:
    anchor-sections: true
    appendix-cite-as:
      - bibtex
    canonical-url: true
    citation: true
    citations-hover: true
    citation-location: document
    css: assets/gt.css
    email-obfuscation: javascript
    fig-format: svg
    footnotes-hover: true
    include-in-header:
      - assets/in-header.html
    keep-md: false
    keywords:
      - referendum
      - Centre for Research on Direct Democracy (C2D)
      - direct democracy
      - report
      - worldwide
    link-external-filter: "^((?:(https?|file):)\\/\\/(creativecommons\\.org|gitlab\\.com\\/zdaarau|orcid\\.org|([a-zA-Z]+\\.)?rdb\\.vote|rdb(\\.report)?\\.rpkg\\.dev|www\\.uzh\\.ch|www\\.zdaarau\\.ch|localhost|127.0.0.1)|mailto:.+@rdb.vote$)"
    link-external-icon: true
    link-external-newwindow: true
    html-math-method:
      method: katex
      url: assets/npm/node_modules/katex/dist/
    # cf. https://quarto.org/docs/output-formats/html-themes.html#theme-options
    theme:
      - litera
      - assets/custom_litera.scss
    toc-depth: 6
    toc-title: "On this page"
  pdf:
    # classoption:
    #   - twocolumn
    documentclass: scrreprt
    fig-align: center
    fig-format: pdf
    filters:
      # cf. https://stackoverflow.com/a/74137244/7196903
      - assets/remove_title.lua
    footnotes-pretty: true
    geometry:
      # reference: http://mirrors.ctan.org/macros/latex/contrib/geometry/geometry.pdf
      - centering=true
      - hscale=0.55
      - vscale=0.86
      # - heightrounded # uncomment this if an `Underfull \vbox` warning should occur
      # - showframe # useful for debugging layout issues
    hyperrefoptions:
      - linktoc=all
      - pdfwindowui
    include-in-header:
      - assets/in-header.tex
    keep-md: false
    keep-tex: false
    link-citations: true
    mainfont: AlegreyaSans
    mainfontoptions:
      - Path=assets/fonts/ttf/AlegreyaSans/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
      - ItalicFont=*-Italic
      - BoldItalicFont=*-BoldItalic
    monofont: FiraCode
    monofontoptions:
      - Path=assets/fonts/ttf/FiraCode/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
    papersize: a4
    sansfont: BigShouldersDisplay
    sansfontoptions:
      - Path=assets/fonts/ttf/BigShouldersDisplay/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
    template: assets/template.latex
    toc-depth: 6
    linkcolor: "DarkSlateBlue"
    filecolor: "DarkSlateBlue"
    citecolor: "DarkSlateBlue"
    urlcolor: "DarkSlateBlue"
    toccolor: "DarkSlateBlue"

knitr:
  opts_chunk:
    crop: false
    echo: false
    # cf. https://statr.me/2014/07/showtext-with-knitr/; most likely redundant with `showtext::showtext_auto()` in `.Rprofile`
    # fig.showtext: true
    message: false
    warning: false
    out.width: "100%"
    # R.options:
    #   some.option: "some value"
  opts_knit:
    progress: false

execute:
  # we don't (yet) activate [knitr caching](https://quarto.org/docs/reference/cells/cells-knitr.html#cache) [globally](https://quarto.org/docs/projects/code-execution.html#cache)
  cache: false

custom:
  has_tbls: false # set this to `true` if the current report edition's print version includes tables
  incl_part_overall: false
  incl_part_national_CH: true
