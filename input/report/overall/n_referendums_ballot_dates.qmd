# Number and share of referendums and ballot dates {#sec-overall-n_rfrnds_ballot_dates}

```{r}
data <-
  rdb.report::dataset() %>% 
  dplyr::filter(level == "national")
```

## National referendums and ballot dates per decade

```{r}
# count the number of national referendums by decade
p_count_dec_nat <-
  ggplot2::ggplot(data = data ,
                  mapping = ggplot2::aes(decade)) + 
  ggplot2::geom_bar(stat = "count",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of referendums:", nrow(data))) +
  ggplot2::scale_x_continuous(breaks = seq(1800, 2020, 50)) +
  ggplot2::scale_y_continuous(limits = c(0,700),
                              breaks = seq(0, 700, 100)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

# count the number of national referendums by decade WITHOUT Switzerland
data_excl_ch <-
  rdb.report::dataset() %>%
  # omit Switzerland
  dplyr::filter(country_code != "CH" & country_code != "LI") %>% 
  dplyr::filter(level == "national")

p_count_dec_nat_noCH <-
  ggplot2::ggplot(data = data_excl_ch,
                  mapping = ggplot2::aes(decade)) + 
  ggplot2::geom_bar(stat = "count",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of referendums:", nrow(data_excl_ch))) +
  ggplot2::scale_x_continuous(breaks = seq(1800, 2020, 50)) +
  ggplot2::scale_y_continuous(limits = c(0,700),
                              breaks = seq(0, 700, 100)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

###########################
# look at ballot dates, not at observations
data_dates <- rdb.report::dataset() %>% 
  dplyr::filter(level == "national") %>% 
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE)
# result: dataset with 1610 rows


# plot
p_date_dec_nat <-
  ggplot2::ggplot(data = data_dates,
                  mapping = ggplot2::aes(decade)) + 
  ggplot2::geom_bar(stat = "count",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of ballot dates:", nrow(data_dates))) +
  ggplot2::scale_x_continuous(breaks = seq(1800, 2020, 50)) +
  ggplot2::scale_y_continuous(limits = c(0,700),
                              breaks = seq(0, 700, 100)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

# look at ballot dates, not at observations (without Switzerland)
data_dates_excl_ch <-
  data_dates %>%
  dplyr::filter(country_code != "CH" 
                & country_code != "LI" 
                & level == "national")

p_date_dec_nat_noCH <-
  ggplot2::ggplot(data = data_dates_excl_ch,
                  mapping = ggplot2::aes(decade)) + 
  ggplot2::geom_bar(stat = "count",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of ballot dates:", nrow(data_dates_excl_ch))) +
  ggplot2::scale_x_continuous(breaks = seq(1800, 2020, 50)) +
  ggplot2::scale_y_continuous(limits = c(0,700),
                              breaks = seq(0, 700, 100)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

###
# calculate the share of number of referendums per ballot date (omitted from output)
share_ref <-
  rdb.report::dataset(part = "overall") %>% 
  dplyr::group_by(decade) %>% 
  dplyr::count() %>% 
  dplyr::rename("n_ref" = n)

share_bd <-
  data_dates %>% 
  dplyr::group_by(decade) %>% 
  dplyr::count() %>% 
  dplyr::rename("n_bd" = n)

# merge
share <- merge(share_ref, share_bd, by = "decade")
# calculate share of referendums per ballot date per decade
share$share <- share$n_ref / share$n_bd
# plot this
p_share <-
  ggplot2::ggplot(data = share,
                  mapping = ggplot2::aes(x = decade,
                                         y = share,
                                         fill = "grey")) +
  ggplot2::geom_col() +
  ggplot2::scale_fill_manual(values = c("#3b528b")) +
  ggplot2::scale_x_continuous(breaks = seq(1900, 2010, 10)) +
  ggplot2::scale_y_continuous(limits = c(0,3),
                              breaks = seq(0, 3, 0.5)) +
  salim::ggplot2_theme(legend.position = "none",
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

@fig-plot-count-dec shows the number of national referendums and ballot dates per decade from 1790 until today. The number of referendums (a) fluctuated at low
levels in the 19th century. In the 20th century, it has increased steadily from around 50 in the decade from 1900 to 1909 to over 600 in the decade from 1990 to
1999. Since then, the number has decreased to slightly more than 400 per decade from 2010 to 2019. In total, `r nrow(data)` referendums have been held worldwide
since 1790.

Once we **exclude Switzerland (CH) and Liechtenstein (LI)** from the analysis (b), the number of referendums worldwide since 1790 is reduced to
`r nrow(data_excl_ch)`. Nevertheless, the general trends are the same: Having omitted Switzerland and Liechtenstein, we see a peak at around 500 referendums in
the 1990s, with a bit of a decline since then.

Looking at [ballot dates](../glossary.qmd#ballot-date), the picture is more evened-out, but the general trends are similar. There is an increase to a peak of
around 250 ballot dates in the decade from 1990 to 1999 (c), with a small decrease for the decades afterwards. Excluding Switzerland and Liechtenstein (d), the
pattern is the same but the total number of ballot dates is reduced from `r nrow(data_dates)` to `r nrow(data_dates_excl_ch)`.

```{r}
#| label: fig-plot-count-dec
#| fig-cap: "Number of national referendums and ballot dates per decade since 1790"
#| fig-subcap: ["Referendums", "Referendums (without CH & LI)", "Ballot dates", "Ballot dates (without CH & LI)"]
#| layout-ncol: 2
#| fig-column: page
#| fig-height: 3

p_count_dec_nat
p_count_dec_nat_noCH
p_date_dec_nat
p_date_dec_nat_noCH
```

## Number and share of countries holding referendums

```{r}
# create dataset with unique countries per decade

countries_unique_dec <-
  data %>% 
  dplyr::group_by(decade) %>%
  dplyr::filter(decade >= 1900) %>%
  dplyr::summarise(ref_countries = dplyr::n_distinct(country_name),
                   .groups = "drop")

# how many countries worldwide per deacde?
cowstates <-
  states::cowstates %>%
  tibble::as_tibble() %>%
  # add start/end years
  dplyr::mutate(year_start = clock::get_year(start),
                year_end = clock::get_year(end)) %>% 
  dplyr::select(cowc,
                country_name,
                year_start,
                year_end)

# calculate number of states per decade
# source: https://stackoverflow.com/questions/13596139/generate-combination-of-data-frame-and-vector
decade <- rev(unique(rdb.report::dataset(part = "overall")$decade))

# expanded data frame
cowstates_exp <- merge(cowstates, as.data.frame(decade))

# add filter for newstates < 9 yrs
cowstates_exp <- cowstates_exp %>% 
  dplyr::mutate(diff = decade - year_start)

cowstates_exp <- cowstates_exp %>% 
  dplyr::mutate(exist = dplyr::case_when(year_start < decade & year_end > decade ~ TRUE,
                                         diff > -10 ~ TRUE,
                                         TRUE ~ FALSE))

cowstates_exp %<>% dplyr::filter(exist)

# unique by decade
cowstates_unique_dec <-
  cowstates_exp %>%
  dplyr::group_by(decade) %>%
  dplyr::summarise(all_countries = dplyr::n_distinct(country_name),
                   .groups = "drop")

# merge the two
countries <- merge(countries_unique_dec, 
                   cowstates_unique_dec, 
                   by = "decade")

# calculate percentage
countries$share <- countries$ref_countries / countries$all_countries * 100

# plot countries
p_countries <-
  ggplot2::ggplot(data = countries,
                  mapping = ggplot2::aes(x = decade,
                                         y = ref_countries,
                                         fill = "grey")) +
  ggplot2::geom_col() +
  ggplot2::scale_fill_manual(values = c("#3b528b")) +
  ggplot2::scale_x_continuous(breaks = seq(1900, 2020, 20)) +
  ggplot2::scale_y_continuous(limits = c(0,110),
                              breaks = seq(0, 100, 10)) +
  salim::ggplot2_theme(base_size = 18,
                       legend.position = "none",
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

# plot share
p_share_countries <-
  ggplot2::ggplot(data = countries,
                  mapping = ggplot2::aes(x = decade,
                                         y = share,
                                         fill = "blue")) +
  ggplot2::geom_col() +
  ggplot2::scale_fill_manual(values = c("#3b528b")) +
  ggplot2::scale_x_continuous(limits = c(1900, 2020),
                              breaks = seq(1900, 2020, 20)) +
  ggplot2::scale_y_continuous(limits = c(0,100),
                              breaks = seq(0, 100, 10),
                              labels = rdb.report::label_percent(scale = 1)) +
  salim::ggplot2_theme(base_size = 18,
                       legend.position = "none",
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

@fig-plot-count-countries shows the number of countries that held referendums at least once a decade. It has increased tenfold since 1900, from around 10 in the
1900s to over 100 from 1990 to 2000 (a). If we control for the number of countries worldwide [@Beger.2021; @cow2017], we find that the highest share was in the
1950s and the 1990s, when around half of all countries worldwide held at least one referendum (b).

```{r}
#| label: fig-plot-count-countries
#| fig-cap: "Number and share of countries holding referendums since 1900"
#| fig-subcap: ["number of countries", "share of countries"]
#| layout-ncol: 2
#| fig-column: page

p_countries
p_share_countries
```

## Top-ten analysis

```{r}
# dataset creation
###
# referendums
data_top_ten <-
  data %>%
  dplyr::filter(decade >= 1900) %>%
  dplyr::group_by(country_name) %>%
  dplyr::summarise(n = dplyr::n(),
                   .groups = "drop") %>%
  dplyr::arrange(desc(n)) %>%
  # reduce to top-ten
  dplyr::filter(dplyr::row_number() %in% 1:10) %>%
  # convert country name to factor in order to enforce current order
  dplyr::mutate(country_name = as.factor(country_name))

###
# ballot dates
data_top_ten_ballot_dates <-
  data %>% 
  dplyr::filter(decade >= 1900) %>%
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE)

data_top_ten_dates <-
  data_top_ten_ballot_dates %>%
  dplyr::group_by(country_name) %>%
  dplyr::summarise(n = dplyr::n(),
                   .groups = "drop") %>%
  dplyr::arrange(desc(n)) %>%
  # reduce to top-ten
  dplyr::filter(dplyr::row_number() %in% 1:10) %>%
  # convert country name to factor in order to enforce current order
  dplyr::mutate(country_name = as.factor(country_name)) 
```

Looking at the top-ten countries with the most referendums (a), we see that Switzerland held by far the most referendums since 1900, followed by New Zealand and
Liechtenstein. Looking at ballot dates (b), Switzerland is still the lone frontrunner, with Liechtenstein coming in second and New Zealand third. It becomes
apparent from @fig-top-ten, that only ten countries are responsible for half the referendums and a third of all ballot dates worldwide since 1900.

```{r}
#| include: false

# plot top-10 countries from 1900 to 2021

# choose viridis palette
# https://waldyrious.net/viridis-palette-generator/

# plot top 10 referendums
p_top_ten <-
  ggplot2::ggplot(data = data_top_ten,
                  mapping = ggplot2::aes(x = n,
                                         y = reorder(country_name, n))) +
  ggplot2::geom_bar(stat = "identity",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of referendums:", sum(data_top_ten$n))) +
  ggplot2::scale_x_continuous(limits = c(0, 650),
                              breaks = c(seq(0, 600, 100))) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##############################
# plot top 10 ballot dates
p_top_ten_dates <-
  ggplot2::ggplot(data = data_top_ten_dates,
                  mapping = ggplot2::aes(x = n,
                                         y = reorder(country_name, n))) +
  ggplot2::geom_bar(stat = "identity",
                    fill = "#3b528b") +
  ggplot2::labs(caption = paste("Total number of ballot dates:", sum(data_top_ten_dates$n))) +
  ggplot2::scale_x_continuous(limits = c(0, 650),
                              breaks = c(seq(0, 600, 100))) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-top-ten
#| fig-cap: "Top-ten countries since 1900"
#| fig-subcap:
#|   - "by number of referendums"
#|   - "by ballot dates"
#| layout-ncol: 2
#| fig-column: page

p_top_ten
p_top_ten_dates
```

## Population size and ballot dates per country

With regard to direct democracy, there has been an ongoing debate on whether countries with smaller populations hold referendums more often than countries with
larger populations. In line with the findings of @Anckar2004 and @Vatter.2000, the analysis in @fig-plot-size shows that there is no correlation between a
country's population size (data by @worldbankdata) and its number of ballot dates per decade. With the exception of a few outliers, mainly from Switzerland and
Liechtenstein, we don't observe more referendums (y-axis) in countries with a small population (x-axis). In fact, even larger countries can have a relatively
high number of referendums per decade, e.g. Egypt in the 1970s.

```{r}
# dataset creation

## get population data for each country (world bank)
data_pop <- WDI::WDI(country = "all", indicator = "SP.POP.TOTL", start = 1960, extra = TRUE)

# create a data frame with count of ballot dates per decade
data_date_decade <-
  rdb.report::dataset(part = "overall") %>% 
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE) %>% 
  dplyr::filter(year >= 1900 & year < 2020) %>%
  dplyr::group_by(country_name,
                  country_code, 
                  decade) %>% 
  dplyr::count() %>%
  dplyr::ungroup()

## average population per country per decade
data_pop_decade <-
  data_pop %>% 
  dplyr::filter(region != "Aggregates") %>% 
  dplyr::select(country, iso2c, year, SP.POP.TOTL) %>% 
  dplyr::rename(population = SP.POP.TOTL) %>% 
  dplyr::mutate(decade = floor(year/10)*10) %>%
  dplyr::summarise(mean_population = mean(population, na.rm = TRUE),
                   .by = c(country,
                           iso2c, 
                           decade)) %>%
  dplyr::filter(decade != 2020)

## combine population data 2020 and ballot dates
data_pop_ballot_date_decade <- 
  dplyr::full_join(data_date_decade, data_pop_decade, 
                   by = c("country_code" = "iso2c", "decade" = "decade"))

## prepare data for analysis
data_size_decade <-
  data_pop_ballot_date_decade %>% 
  dplyr::mutate(country_name = dplyr::coalesce(country_name, country)) %>% 
  dplyr::select(-country) %>% 
  dplyr::mutate(n = ifelse(is.na(n), 0, n),
                label = paste(country_name, decade))

## Scatterplot
p_size <-
  ggplot2::ggplot(data = data_size_decade,
                  mapping = ggplot2::aes(x = mean_population,
                                         y = n)) +
  ggplot2::geom_point() +
  ggrepel::geom_text_repel(mapping = ggplot2::aes(label = ifelse(n >= 8, as.character(label), '')), 
                           size = 2,
                           max.iter = 50000) +
  ggplot2::scale_x_log10() +
  ggplot2::xlab("Population size (log)") +
  ggplot2::ylab("Referendums per decade") +
  salim::ggplot2_theme() +
  salim::ggplot2_theme_html()

## 2D-histogram
p_size_hist <-
  ggplot2::ggplot(data = data_size_decade,
                  mapping = ggplot2::aes(x = log_pop,
                                         y = n)) +
  ggplot2::geom_bin2d(bins = 15) +
  ggplot2::xlab("Population size (log)") +
  ggplot2::guides(fill = ggplot2::guide_legend(title = "Number of countries:")) +
  salim::ggplot2_theme(axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-plot-size
#| fig-cap: "Number of referendums per decade by population size"
#| fig-column: page

p_size
```
