# Referendum and regime type {#sec-overall-rfrnd_regime_type}

## Referendum type worldwide

```{r}
#| include: false

# calculate type
rdb_selection_type <-
  rdb.report::dataset() %>% 
  dplyr::filter(level == "national"
                & year >= 1900
                & !is.na(inst_trigger_type)) %>%  
  dplyr::group_by(decade, inst_trigger_type) %>%
  dplyr::summarise(freq = dplyr::n(),
                   .groups = "drop")  %>%
  dplyr::mutate(perc = freq / sum(freq) * 100)

# graph overall
n_rfrnds_per_inst_trigger_type <-
  ggplot2::ggplot(data = rdb_selection_type,
                  mapping = ggplot2::aes(x = decade,
                                         y = freq)) +
  ggplot2::geom_line(mapping = ggplot2::aes(colour = inst_trigger_type),
                     linewidth = 1) +
  ggplot2::scale_color_manual("",
                              values = c("darkgrey", "lightblue", "darkblue")) +
  ggplot2::scale_x_continuous(breaks = seq(1900, 2020, 20)) + 
  ggplot2::scale_y_continuous(breaks = seq(0,260, 50)) +
  ggplot2::labs(caption = paste("Total number of referendums: ",
                                sum(rdb_selection_type$freq))) +
  salim::ggplot2_theme(axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

#################################
# by region
rdb_selection_type_reg <-
  rdb.report::dataset() %>% 
  dplyr::filter(level == "national"
                & year >= 1900
                & !is.na(inst_trigger_type)) %>% 
  dplyr::group_by(decade, inst_trigger_type, un_region_tier_1_name) %>%
  dplyr::summarise(freq = dplyr::n(),
                   .groups = "drop")  %>%
  dplyr::mutate(perc = freq / sum(freq) * 100)

# plot per region
for (region in unique(rdb_selection_type_reg$un_region_tier_1_name)) {
  
  data <-
    rdb_selection_type_reg |>
    dplyr::filter(un_region_tier_1_name == !!region)
  
  assign(x = paste0("n_rfrnds_per_inst_trigger_type_", tolower(region)),
         value =
           data |>
           ggplot2::ggplot(mapping = ggplot2::aes(x = decade,
                                                  y = freq)) +
           ggplot2::geom_line(mapping = ggplot2::aes(colour = inst_trigger_type),
                              linewidth = 1) +
           ggplot2::scale_color_manual("",
                                       values = c("darkgrey", "lightblue", "darkblue")) +
           ggplot2::scale_x_continuous(breaks = seq(from = pal::safe_min(rdb_selection_type_reg$decade),
                                                    to = pal::safe_max(rdb_selection_type_reg$decade),
                                                    by = 50L)) + 
           ggplot2::scale_y_continuous(labels = scales::label_number(accuracy = 1)) +
           ggplot2::labs(caption = paste("Total number of referendums: ", sum(data$freq))) +
           salim::ggplot2_theme(base_size = 18,
                                axis.title.x = ggplot2::element_blank(),
                                axis.title.y = ggplot2::element_blank()) +
           salim::ggplot2_theme_html())
}
```

In @fig-n-rfrnds-per-inst-trigger-type and @fig-n-rfrnds-per-inst-trigger-type-region, we analyze the [institutional trigger
type](../glossary.qmd#inst-trigger-type) of referendums worldwide since 1900. This variable denotes the way in which a referendum was initiated. As we can see,
the differences between the world regions are quite pronounced:

-   In **Africa** (a), most referendums were *top-down* and some *automatic*. There were no *bottom-up referendums*.
-   In the **Americas** (b), most referendums were *top-down*, with some *automatic* and few *bottom-up* referendums.
-   In **Asia** (c), most referendums were either *automatic* or *top-down*. The number of *top-down* referendums remained low.
-   In **Europe** (d), the frequency of *top-down* and *automatic* referendums was similar. The number of *bottom-up* referendums peaked at the turn of the
    millennium.
-   In **Oceania** (e), there were many *automatic* referendums and few *bottom-up* and even fewer *top-down* referendums.

In summary, *top-down* referendums dominated in Africa and the Americas. In Asia and Oceania, there was an increase in *automatic* referendums. In Europe, there
were the most *bottom-up* referendums, and the number of *top-down* and *automatic* referendums was similar.

```{r}
#| label: fig-n-rfrnds-per-inst-trigger-type
#| fig-cap: "Referendums by institutional trigger type per decade since 1900"
#| fig-column: page
#| fig-height: 3

n_rfrnds_per_inst_trigger_type
```

```{r}
#| label: fig-n-rfrnds-per-inst-trigger-type-region
#| fig-cap: "Referendums by institutional trigger type and region per decade since 1900"
#| fig-subcap: ["Africa", "Americas", "Asia", "Europe", "Oceania"]
#| layout-ncol: 3
#| fig-column: page

n_rfrnds_per_inst_trigger_type_africa
n_rfrnds_per_inst_trigger_type_americas
n_rfrnds_per_inst_trigger_type_asia
n_rfrnds_per_inst_trigger_type_europe
n_rfrnds_per_inst_trigger_type_oceania
```

## Referendum type by regime type

```{r}
#| include: false

# we need the following variables:
# country_text_id -> ISO-3 countrycode
# year -> year
# v2x_regime -> regime type

regime <-
  vdemdata::vdem %>%
  dplyr::select(country_code_long = country_text_id,
                year,
                v2x_regime,
                v2x_libdem) %>%
  dplyr::filter(year >= 1900) %>%
  dplyr::mutate(regimetype = dplyr::case_when(v2x_regime == 0 ~ "closed autocracy",
                                              v2x_regime == 1 ~ "electoral autocracy",
                                              v2x_regime == 2 ~ "electoral democracy",
                                              v2x_regime == 3 ~ "liberal democracy")) %>%
  tibble::as_tibble()

# merge V-Dem with rdb data using country codes
merged_selection <-
  rdb.report::dataset() %>% 
  dplyr::filter(level == "national"
                & year >= 1900) %>% 
  rdb::add_country_code_long() %>%
  dplyr::left_join(y = regime,
                   by = c("country_code_long", "year"))

# look at problematic cases
# merged_problems <- merged_selection %>% 
#dplyr::filter(is.na(v2x_regime)) 

# country_vec <- unique(merged_problems$country_name)
# country_vec

# calculate type
# by RoW
rdb_selection_type_RoW <-
  merged_selection %>%
  dplyr::filter(!is.na(inst_trigger_type)) %>% 
  dplyr::group_by(decade, inst_trigger_type, regimetype) %>%
  dplyr::summarise(freq = dplyr::n(),
                   .groups = "drop") %>%
  tidyr::drop_na(decade)

# plot per region
for (regime_type in unique(rdb_selection_type_RoW$regimetype)) {
  
  data <-
    rdb_selection_type_RoW |>
    dplyr::filter(regimetype == !!regime_type)
  
  assign(x = paste0("n_rfrnds_per_inst_trigger_type_", stringr::str_replace_all(string = tolower(regime_type),
                                                                                pattern = "\\s",
                                                                                replacement = "_")),
         value =
           data |>
           ggplot2::ggplot(mapping = ggplot2::aes(x = decade,
                                                  y = freq)) +
           ggplot2::geom_line(mapping = ggplot2::aes(colour = inst_trigger_type),
                              linewidth = 1) +
           ggplot2::scale_color_manual("type",
                                       values = c("darkgrey", "lightblue", "darkblue")) +
           ggplot2::scale_x_continuous(breaks = seq(1900, 2020, 50)) + 
           ggplot2::scale_y_continuous(labels = scales::label_number(accuracy = 1)) +
           ggplot2::labs(caption = paste("n=", sum(data$freq))) +
           salim::ggplot2_theme(base_size = 18,
                                axis.title.x = ggplot2::element_blank(),
                                axis.title.y = ggplot2::element_blank()) +
           salim::ggplot2_theme_html())
}
```

In @fig-n-rfrnds-per-inst-trigger-type-regime-type, we differentiate the **type** of referendum by **regime type**, using the Regimes of the World (RoW) measure
by V-Dem [@Coppedge.2023]. We find the following:

-   In **closed autocracies** (a), mostly *top-down* and a few *automatic* referendums were held. There were only three *bottom-up* referendums in closed
    autocracies.

-   In **electoral autocracies** (b), the number of *top-down* and *automatic* referendums was similar and higher than in closed autocracies. Again, there were
    almost no *bottom-up* referendums.

-   In **electoral democracies** (c), again mostly *top-down* referendums were held. The number of *automatic* and *bottom-up* referendums was about equal.

-   In **liberal democracies** (d), the number of *bottom-up* referendums was highest, followed by *automatic* referendums and then *top-down* referendums.

-   The number of referendums in countries without a *Regimes of the World* value (NAs) was very high. This is due to the fact that the RoW measure is not
    available for many of the small island states making up the bulk of *automatic* referendums.

In summary, we see that referendums took place across democracies and autocracies. However, the **trigger type** varies. *Top-down* referendums dominate in
*closed autocracies* and *electoral democracies*. The number of *top-down* and *automatic* referendums is similar in *electoral autocracies* and *liberal
democracies*. *Bottom-up* referendums were almost only observed in *liberal democracies*.

```{r, eval = FALSE}
# which *closed autocracies* held bottom up referendums?
investigate <- merged_selection %>% 
  dplyr::filter(regimetype == "closed autocracy" & inst_trigger_type == "bottom up") %>% 
  dplyr::select(country_name,
                year,
                regimetype,
                title_en)
```

```{r}
#| label: fig-n-rfrnds-per-inst-trigger-type-regime-type
#| fig-cap: "Referendums by regime type and region per decade since 1900"
#| fig-subcap: ["closed autocracy", "electoral autocracy", "electoral democracy", "liberal democracy"]
#| layout-ncol: 2
#| fig-column: page

n_rfrnds_per_inst_trigger_type_closed_autocracy
n_rfrnds_per_inst_trigger_type_electoral_autocracy
n_rfrnds_per_inst_trigger_type_electoral_democracy
n_rfrnds_per_inst_trigger_type_liberal_democracy
```

## Rank number of ballot dates by regime type (V-Dem)

```{r}
#| include: false

max_n <-
  merged_selection %>%
  dplyr::filter(!is.na(country_name)
                & country_code != "CH"
                & country_code != "LI") %>%
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE) %>%
  dplyr::group_by(regimetype, country_name) %>%
  dplyr::summarise(n = dplyr::n(),
                   .groups = "drop") %$%
  pal::safe_max(n)

merged_selection_dates <-
  merged_selection %>% 
  dplyr::filter(!is.na(country_name)
                & country_code != "CH"
                & country_code != "LI") %>%
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE)

##########################
# rank liberal democracies
ranking_dates_libdem <-
  merged_selection_dates %>%
  dplyr::filter(regimetype == "liberal democracy") %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_libdem <- ranking_dates_libdem[1:10, ]

# reorder factors
top_ten_dates_libdem$country_name <- factor(top_ten_dates_libdem$country_name,
                                            levels = top_ten_dates_libdem$country_name[order(top_ten_dates_libdem$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_libdem <-
  ggplot2::ggplot(data = top_ten_dates_libdem,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "navyblue") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates %>% 
                                       dplyr::filter(regimetype == "liberal democracy"
                                                     & !is.na(country_name)
                                                     & country_code != "CH")))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank electoral democracies
ranking_dates_eldem <-
  merged_selection_dates %>%
  dplyr::filter(regimetype == "electoral democracy") %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_eldem <- ranking_dates_eldem[1:10, ]

# reorder factors
top_ten_dates_eldem$country_name <- factor(top_ten_dates_eldem$country_name,
                                           levels = top_ten_dates_eldem$country_name[order(top_ten_dates_eldem$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_eldem <-
  ggplot2::ggplot(data = top_ten_dates_eldem,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "steelblue") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates %>% 
                                       dplyr::filter(regimetype == "electoral democracy"
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank electoral autocracies
ranking_dates_elaut <-
  merged_selection_dates %>%
  dplyr::filter(regimetype == "electoral autocracy") %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_elaut <- ranking_dates_elaut[1:10, ]

# reorder factors
top_ten_dates_elaut$country_name <- factor(top_ten_dates_elaut$country_name,
                                           levels = top_ten_dates_elaut$country_name[order(top_ten_dates_elaut$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_elaut <-
  ggplot2::ggplot(data = top_ten_dates_elaut,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "indianred3") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates %>% 
                                       dplyr::filter(regimetype == "electoral autocracy"
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank closed autocracies
ranking_dates_claut <-
  merged_selection_dates %>%
  dplyr::filter(regimetype == "closed autocracy") %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_claut <- ranking_dates_claut[1:10, ]

# reorder factors
top_ten_dates_claut$country_name <- factor(top_ten_dates_claut$country_name,
                                           levels = top_ten_dates_claut$country_name[order(top_ten_dates_claut$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_claut <-
  ggplot2::ggplot(data = top_ten_dates_claut,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "darkred") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates %>% 
                                       dplyr::filter(regimetype == "closed autocracy"
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank NAs for completeness
ranking_dates_NA <-
  merged_selection_dates %>%
  dplyr::filter(is.na(regimetype)) %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_NA <- ranking_dates_NA[1:10, ]

# reorder factors
top_ten_dates_NA$country_name <- factor(top_ten_dates_NA$country_name,
                                        levels = top_ten_dates_NA$country_name[order(top_ten_dates_NA$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_NA <-
  ggplot2::ggplot(data = top_ten_dates_NA,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "lightgrey") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ",
                                nrow(merged_selection_dates %>% 
                                       dplyr::filter(is.na(regimetype)
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

@fig-top-ten-by-regimetype-vdem shows the number of ballot dates differentiated by Regimes of the World (RoW) [@Coppedge.2023]:

-   Around 260 ballot dates took place in *liberal democracies*. Excluding Switzerland with close to 300 ballot dates, the frontrunners were New Zealand (39),
    Ireland (31) and Italy (23). Note that Lithuania is among the top-ten for both *liberal* (7) and *electoral democracies* (5).
-   A total of around 150 ballot dates took place in *electoral democracies*. Here, the frontrunners are Ecuador and Uruguay.
-   Around 260 ballot dates took place in *electoral autocracies*. The most frequent countries were Egypt (18), Syria (11) and Zimbabwe and the Philippines with
    9 each.
-   Around 200 ballot dates took place in *closed autocracies*. Here, the most frequent countries were the Maldives (13), Morocco (11) and again Egypt (9).
-   For around 270 ballot dates, there was no information concerning regime type available. This is where the country names could not be matched with the RoW
    dataset. This can be due to sovereign states not coded by RoW (e.g. Liechtenstein, Palau or Micronesia) or it can be due to RDB coding territorial units
    that are not sovereign states (e.g. Norfolk Island: Australia, Northern Mariana Islands: USA, U.S. Virgin Islands, etc.).

```{r}
#| label: fig-top-ten-by-regimetype-vdem
#| fig-cap: "Top-ten countries since 1900 by number of ballot dates and regime type (RoW), without Switzerland and Liechtenstein"
#| fig-subcap: ["liberal democracies", "electoral democracies", "electoral autocracies", "closed autocracies", "NA"]
#| layout-ncol: 2
#| fig-column: page

p_top_ten_dates_libdem
p_top_ten_dates_eldem
p_top_ten_dates_elaut
p_top_ten_dates_claut
p_top_ten_dates_NA
```

## Rank number of ballot dates by regime type (Freedom House)

In @fig-top-ten-by-regimetype-fh, we re-run the analysis according to **type** of referendums by **regime type**, this time using the Freedom House measure of
regime type [@FreedomHouse.2023]. Because Freedom House only gathered data since 1973, the overall numbers are lower and the ranking order changes. What is new
with the Freedom House data is that smaller countries such as **San Marino** (*free*), the **Comoros** (*partly free*) and the **Maldives** (*not free*) are
added to the list, thus making for a more complete analysis.

However, even using Freedom House data, there were more than 230 cases (NA) where there was no measure of regime type available for a given year.

```{r}
#| include: false
# we need the following variables:
# country_text_id -> ISO-3 countrycode
# year -> year
# Freedom House data: https://xmarquez.github.io/democracyData/reference/download_fh.html
# remotes::install_github("xmarquez/democracyData")

fh <- 
  democracyData::download_fh(verbose = FALSE) %>% 
  dplyr::mutate(status_long = dplyr::case_when(status == "NF" ~ "not free",
                                               status == "PF" ~ "partly free",
                                               status == "F" ~ "free"))
fh <-
  fh %>% 
  dplyr::mutate(country_code_long = countrycode::countryname(fh_country,
                                                             destination = 'iso3c')) %>% 
  dplyr::select(country_code_long,
                year,
                status_long)

# merge Freedom House with rdb data using country codes
merged_selection_fh <-
  rdb.report::dataset() %>% 
  dplyr::filter(level == "national"
                & year >= 1900) %>%  
  rdb::add_country_code_long() %>%
  dplyr::left_join(y = fh,
                   by = c("country_code_long", "year"))
```

```{r}
# calculate type
# by FH
rdb_selection_type_fh <-
  merged_selection_fh %>%
  dplyr::group_by(decade, inst_trigger_type, status_long) %>%
  dplyr::summarise(freq = dplyr::n(),
                   .groups = "drop") %>%
  tidyr::drop_na(decade)

# graph per region (facet wrap)
p_type_dec_fh <-
  ggplot2::ggplot(data = rdb_selection_type_fh,
                  mapping = ggplot2::aes(x = decade,
                                         y = freq)) +
  ggplot2::geom_line(mapping = ggplot2::aes(colour = inst_trigger_type),
                     linewidth = 1) +
  ggplot2::scale_color_manual("type",
                              values = c("darkgrey", "lightblue", "darkblue")) +
  ggplot2::scale_x_continuous(breaks = seq(from = pal::safe_min(rdb_selection_type_fh$decade),
                                           to = pal::safe_min(rdb_selection_type_fh$decade),
                                           by = 50L)) + 
  ggplot2::scale_y_continuous(labels = scales::label_number(accuracy = 1)) +
  ggplot2::labs(caption = paste("n=", sum(rdb_selection_type_fh$freq))) +
  ggplot2::facet_wrap(~ status_long) +
  salim::ggplot2_theme(axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

```{r}
#| include: false
#| label: fig-top-ten-by-regimetype
#| fig-cap: "Top-ten countries 1900–2021 by number of ballot dates and regime type"
#| fig-subcap:
#|   - "liberal democracies (without Switzerland)"
#|   - "electoral democracies"
#|   - "electoral autocracies"
#|   - "closed autocracies"
#|   - "N/A"

p_type_dec_fh
```

```{r}
#| include: false

max_n_fh <-
  merged_selection_fh %>%
  dplyr::filter(!is.na(country_name)
                & country_code != "CH"
                & country_code != "LI") %>%
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE) %>%
  dplyr::group_by(status_long, country_name) %>%
  dplyr::summarise(n = dplyr::n(),
                   .groups = "drop") %$%
  pal::safe_max(n)

merged_selection_dates_fh <-
  merged_selection_fh %>% 
  dplyr::filter(!is.na(country_name)
                & country_code != "CH"
                & country_code != "LI") %>%
  dplyr::distinct(date,
                  country_name,
                  .keep_all = TRUE)

##########################
# rank free
ranking_dates_free <-
  merged_selection_dates_fh %>%
  dplyr::filter(status_long == "free"
                & !is.na(country_name)) %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_free <- ranking_dates_free[1:10, ]

# reorder factors
top_ten_dates_free$country_name <- factor(top_ten_dates_free$country_name,
                                          levels = top_ten_dates_free$country_name[order(top_ten_dates_free$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_free <-
  ggplot2::ggplot(data = top_ten_dates_free,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "#3cac88") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates_fh %>% 
                                       dplyr::filter(status_long == "free"
                                                     & !is.na(country_name)
                                                     & country_code != "CH")))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank partly free
ranking_dates_partly_free <-
  merged_selection_dates_fh %>%
  dplyr::filter(status_long == "partly free"
                & !is.na(country_name)) %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_partly_free <- ranking_dates_partly_free[1:10, ]

# reorder factors
top_ten_dates_partly_free$country_name <- factor(top_ten_dates_partly_free$country_name,
                                                 levels = top_ten_dates_partly_free$country_name[order(top_ten_dates_partly_free$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_partly_free <-
  ggplot2::ggplot(data = top_ten_dates_partly_free,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "#b9a350") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates_fh %>% 
                                       dplyr::filter(status_long == "partly free"
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()

##########################
# rank not free
ranking_dates_not_free <-
  merged_selection_dates_fh %>%
  dplyr::filter(status_long == "not free"
                & !is.na(country_name)) %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_not_free <- ranking_dates_not_free[1:10, ]

# reorder factors
top_ten_dates_not_free$country_name <- factor(top_ten_dates_not_free$country_name,
                                              levels = top_ten_dates_not_free$country_name[order(top_ten_dates_not_free$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_not_free <-
  ggplot2::ggplot(data = top_ten_dates_not_free,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "#9869a0") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ", 
                                nrow(merged_selection_dates_fh %>% 
                                       dplyr::filter(status_long == "not free"
                                                     & !is.na(country_name))))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()


##########################
# rank NAs for completeness
ranking_dates_NA_fh <-
  merged_selection_dates_fh %>%
  dplyr::filter(is.na(status_long)
                & !is.na(country_name)
                & year > 1972) %>%
  dplyr::group_by(country_name) %>%
  dplyr::count() %>%
  dplyr::arrange(desc(n))

# select the top-five
top_ten_dates_NA_fh <- ranking_dates_NA_fh[1:10, ]

# reorder factors
top_ten_dates_NA_fh$country_name <- factor(top_ten_dates_NA_fh$country_name,
                                           levels = top_ten_dates_NA_fh$country_name[order(top_ten_dates_NA_fh$n, decreasing = FALSE)])

# plot top 10 ballot dates
p_top_ten_dates_NA_fh <-
  ggplot2::ggplot(data = top_ten_dates_NA_fh,
                  mapping = ggplot2::aes(x = n,
                                         y = country_name)) +
  ggplot2::geom_bar(stat = "identity", fill = "lightgrey") +
  ggplot2::labs(caption = paste("Total number of ballot dates: ",
                                nrow(merged_selection_dates_fh %>% 
                                       dplyr::filter(is.na(status_long)
                                                     & !is.na(country_name)
                                                     & year > 1972)))) +
  ggplot2::scale_x_continuous(labels = scales::label_number(accuracy = 1),
                              limits = c(NA_integer_, max_n)) +
  salim::ggplot2_theme(base_size = 18,
                       axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank()) +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-top-ten-by-regimetype-fh
#| fig-cap: "Top-ten countries since 1973 by number of ballot dates and regime type (Freedom House), without Switzerland and Liechtenstein"
#| fig-subcap: ["free", "partly free", "not free", "NA"]
#| layout-ncol: 2
#| fig-column: page

p_top_ten_dates_free
p_top_ten_dates_partly_free
p_top_ten_dates_not_free
p_top_ten_dates_NA_fh
```
