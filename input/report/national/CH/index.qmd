# Switzerland

::: {.content-visible when-meta="custom.incl_part_national_CH"}
## Focus of our analysis

Switzerland is known for its direct democracy and large number of referendum votes. This is why we have compiled this analysis on Swiss national referendums
since 1793, and Swiss cantonal referendums since 1970. We have not yet recorded earlier cantonal referendums into the *Referendum Database*. Also at the
cantonal level, we have omitted [Landsgemeinde](../../glossary.qmd#landsgemeinde) votes from the analysis, because they have not yet been completely recorded.
This means that we completely exclude the cantons of Appenzell Innerrhoden and Glarus from the analysis. Votes in Nidwalden are included from 1997 onwards,
votes in Appenzell Ausserrhoden from 1998 onwards, and votes in Obwalden from 1999 onwards. Furthermore, our database contains only few referendums at the
municipal level, which is why we did not analyze the municipal level in more detail.

We use the internationally established nomenclature on popular votes, with the term [referendum](../../glossary.qmd#referendum-instance) referring to any
popular vote. Please note that this usage is broader than the common Swiss usage of the term "referendum", which does not encompass citizens' initiatives. In
our analysis, we continue to distinguish between the [referendum](../../glossary.qmd#referendum-instance) as a vote on a single question, and the [ballot
date](../../glossary.qmd#ballot-date) as the date on which one or several referendums are held in a polity.

## Report structure

In the following analysis, we first provide a big picture (@sec-ch-big_picture), showing why Switzerland is unique with regard to the number of referendums.
Afterwards, we compare these counts of referendums at the national and cantonal level (@sec-ch-n_referendums). Subsequently, the report delves into an analysis
of types of referendums (@sec-ch-types), showing various attributes of different types of direct democratic instruments. @sec-ch-topics investigates the various
topics that are voted on in referendums in Switzerland. @sec-ch-turnout then proceeds with an empirical examination of the turnout, while @sec-ch-outcomes
delves deeper into their outcomes. @sec-ch-congruence examines the congruence between governmental/parliamentary recommendations and popular votes. Finally, we
summarize our findings in @sec-ch-conclusion and provide an outlook for future research endeavors.
:::

::: {.content-visible unless-meta="custom.incl_part_national_CH"}
This part of the report sheds light on national and subnational referendums in Switzerland and is only available in the [interactive online
version](https://report.rdb.vote/national/CH/).
:::
