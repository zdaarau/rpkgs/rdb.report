# Turnout {#sec-ch-turnout}

## National and cantonal turnout over time

```{r}
# Data

## national
turnout_national <-
  rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "national") |>
  dplyr::filter(decade >= 1870) |>
  dplyr::group_by(decade) |>
  dplyr::summarize(
    turnout_average = mean(turnout, na.rm = TRUE),
    level = "national")

## cantonal
turnout_subnational <-
  rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "subnational") |>
  dplyr::filter(decade >= 1970) |>
  dplyr::group_by(decade) |>
  dplyr::summarize(
    turnout_average = mean(turnout, na.rm = TRUE),
    level = "cantonal")

## combined datasets
turnout_combined <-
  dplyr::bind_rows(turnout_national, turnout_subnational) |>
  dplyr::mutate(level = factor(level, levels = c("national", "cantonal")))
```

```{r}
# Plot
combined_turnout_plot <-
  turnout_combined |>
  ggplot2::ggplot(ggplot2::aes(x = decade,
                               y = turnout_average, 
                               color = level, 
                               shape = level,
                               group = level)) +
  ggplot2::geom_line(linewidth = 1.2) +
  ggplot2::geom_point(size = 3) +
  ggplot2::geom_text(mapping = ggplot2::aes(label = rdb.report::percent(turnout_average,
                                                                        accuracy = 1),
                                            vjust = ifelse(level == "national", -1.8, 2.8)),
                     size = 3,
                     show.legend = FALSE) +
  ggplot2::scale_color_manual(values = c("#3b528b", "#3ade70")) +
  ggplot2::scale_shape_manual(values = c(15, 16)) + 
  ggplot2::labs(color = ggplot2::element_blank(),
                shape = ggplot2::element_blank()) +
  ggplot2::scale_x_continuous(breaks = seq(1880, 2020, 20)) +
  ggplot2::scale_y_continuous(labels = rdb.report::label_percent(accuracy = 1), 
                              limits = c(0, 0.7)) +
  salim::ggplot2_theme(axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank(),
                       legend.margin = ggplot2::margin(t = 15)) +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-turnout-lineplot
#| fig-cap: "Turnout in national & cantonal referendums by decade, 1870–2024 (national) & 1970–2024 (cantonal)"
#| fig-column: page
#| fig-height: 3.8

combined_turnout_plot
```

@fig-turnout-lineplot illustrates voter turnout in national referendums by decade since 1900. The graph reveals significant fluctuations in turnout rates over
time.

At the beginning of the 20th century, turnout was relatively high, starting at around 55 % but declining slightly to about 50 % by the 1920s. There is a notable
increase in turnout through the 1920s and 1940s, peaking at over 60 % around 1930. This could potentially be the result of heightened political engagement
during the interwar period and World War II.

After the 1940s, turnout steeply declined, reaching its lowest point in the 1980s at around 40 %. While it is not clear where this drop stems from, it could be
attributed to various factors, including political disengagement or a reduction in contentious referendum issues during that time.

Following this low point, turnout has been slowly increasing since the 1980s, and by the 2010s, it had recovered to levels over 50 %. This recent upward trend
may reflect renewed political interest as a result of more topic diversity in referendums (see @sec-ch-topics).

@fig-turnout-lineplot also illustrates voter turnout in cantonal referendums by decade, starting from 1970. The graph shows an upward trend in voter engagement
over time. In the 1970s and 1980s, turnout was relatively low, hovering around 35–40 %, reflecting a period of lower political participation at the cantonal
level. From the 1980s onwards, turnout started to increase steadily. By the 2000s, it rose above 40 %, and in the first year of the 2020s, it surpassed 45 %,
indicating a significant rise in political engagement at the cantonal level over the past few decades.

This steady increase suggests growing voter interest and participation in cantonal referendums, potentially driven by more salient issues being put to vote at
the cantonal level in recent years. Interestingly, it coincides with the decrease in the number of referendums, which could indicate a possible increase in
voter engagement when referendums are scarcer. The rising trend also highlights the general importance of subnational governance in Swiss political life.

## Cantonal turnout

```{r}
# Data

turnout_cantonal <-
  rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "subnational") |>
  dplyr::filter(year >= 1970) |>
  dplyr::group_by(subnational_entity_name) |>
  dplyr::summarize(turnout_average = if (all(is.na(turnout))) NA else mean(turnout, na.rm = TRUE)) |>
  dplyr::mutate(subnational_entity_name = dplyr::recode(
    subnational_entity_name, 
    "Basel Landschaft" = "Basel-Landschaft",
    "Basel Stadt" = "Basel-Stadt",
    "Geneva" = "Genève",
    "Lucerne" = "Luzern",
    "Zurich" = "Zürich"))  |>
  dplyr::bind_rows(tibble::tibble(subnational_entity_name = "Glarus",
                                  turnout_average = NA)) |>
    dplyr::mutate(turnout_average_percent = paste0(round(turnout_average * 100, 
                                                       digits = 1), "%"))

swiss_cantons <-
  sf::st_read(rdb.report::path_pkg_repo("input/report/assets/shapefiles/swissboundaries3d_2024-01_2056_5728.shp/swissBOUNDARIES3D_1_5_TLM_KANTONSGEBIET.shp"),
              quiet = TRUE) |>
  sf::st_zm()

swiss_cantons <- merge(swiss_cantons, turnout_cantonal,
                       by.x = "NAME",
                       by.y = "subnational_entity_name")
```

```{r}
# Plot

turnout_map <-
  ggplot2::ggplot(data = swiss_cantons) +
  ggplot2::geom_sf(ggplot2::aes(fill = turnout_average), color = "black") +
  ggplot2::scale_fill_viridis_c(direction = -1,
                                name = "turnout",
                                labels = rdb.report::label_percent(),
                                na.value = "white") +
  ggplot2::guides(fill = ggplot2::guide_colorbar(title.position = "top",
                                                 title.hjust = 0.5)) +
  ggplot2::coord_sf(datum = NA) +
  salim::ggplot2_theme(axis.title.x = ggplot2::element_blank(),
                       axis.title.y = ggplot2::element_blank(),
                       legend.key.width = grid::unit(0.15, units = "npc"),
                       legend.key.height = grid::unit(0.04, units = "npc")) +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-turnout-map
#| fig-cap: "Turnout in cantonal referendums by canton, 1970–2024"
#| fig-column: page
#| fig-height: 4

turnout_map
```

@fig-turnout-map presents a map of Switzerland illustrating the average voter turnout in cantonal referendums from 1970 onwards. The highest turnout rates by
far are found in Schaffhausen (`{r} turnout_cantonal$turnout_average_percent[turnout_cantonal$subnational_entity_name == "Schaffhausen"]`), where turnout is
traditionally high, due to voting being mandated by Chapter 3 of the Cantonal Constitution [@SchaffhausenConstitution.2021]. All other cantons lie within the
range of `{r} turnout_cantonal$turnout_average_percent[turnout_cantonal$subnational_entity_name == "Valais"]` (Valais) and
`{r} turnout_cantonal$turnout_average_percent[turnout_cantonal$subnational_entity_name == "Appenzell Ausserrhoden"]` (Appenzell Ausserrhoden). This suggests a
moderate level of participation across much of the country.

## Turnout by type

```{r}
# Data

## national
turnout_type_national <- rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "national") |>
  dplyr::filter(decade >= 1970) |>
  dplyr::group_by(type) |>
  dplyr::summarise(average_turnout = mean(turnout, na.rm = TRUE)) |>
  dplyr::mutate(level = "national")

governmental_referendum_national <- data.frame(
  type = "governmental referendum",
  average_turnout = NA,
  level = "national")

turnout_type_national <- turnout_type_national |>
  dplyr::bind_rows(governmental_referendum_national)

## subnational
turnout_type_subnational <- rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "subnational") |>
  dplyr::filter(decade >= 1970) |>
  dplyr::group_by(type) |>
  dplyr::summarise(average_turnout = mean(turnout, na.rm = TRUE)) |>
  dplyr::mutate(level = "cantonal")

## combined
turnout_type_combined <- dplyr::bind_rows(turnout_type_national, turnout_type_subnational) |>
  dplyr::mutate(level = factor(level, levels = c("national", "cantonal")))
```

```{r}
# Plot

turnout_type_plot <-
  ggplot2::ggplot(data = turnout_type_combined, 
                  ggplot2::aes(x = reorder(type,
                                           average_turnout),
                               y = average_turnout, 
                               fill = factor(level))) +
  ggplot2::geom_bar(stat = "identity",
                    position = ggplot2::position_dodge2(reverse = TRUE),
                    width = 0.7) +
  ggplot2::scale_fill_manual(values = c("#3b528b", "#3ade70")) +
  ggplot2::labs(title = ggplot2::element_blank(),
                x = ggplot2::element_blank(),
                y = ggplot2::element_blank(),
                fill = ggplot2::element_blank()) +
  ggplot2::coord_flip() +
  ggplot2::geom_text(mapping = ggplot2::aes(label = rdb.report::percent(average_turnout,
                                                                        accuracy = 0.1)), 
                     position = ggplot2::position_dodge2(width = 0.9, reverse = TRUE), 
                     hjust = -0.1,
                     size = 3L) +
  ggplot2::scale_y_continuous(labels = rdb.report::label_percent(accuracy = 1),
                              expand = ggplot2::expansion(mult = c(0, 0.3))) +
  salim::ggplot2_theme() +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-turnout-type
#| fig-cap: "Turnout in national & cantonal referendums by type, 1970–2024"
#| fig-column: page
#| fig-height: 3

turnout_type_plot
```

@fig-turnout-type depicts the turnout rates by referendum type in referendums at the national and subnational level. As can be seen, the turnout rates are
around 40 % for cantonal referendums and a bit higher for national referendums. The turnout rates do not vary much across referendum types, which is probably
because oftentimes, referendums of different types are held on the same ballot date.

## Turnout by topic

```{r}
# Data

turnout_topic_national <- 
  rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "national") |>
  dplyr::filter(decade >= 1970) |>
  rdb::unnest_var(topics_tier_1) |>
  dplyr::filter(!is.na(topic_tier_1)) |>
  dplyr::group_by(topic_tier_1) |>
  dplyr::summarise(average_turnout = mean(turnout, na.rm = TRUE)) |>
  dplyr::mutate(level = "national")

turnout_topic_cantonal <- 
  rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "subnational") |>
  dplyr::filter(decade >= 1970) |>
  rdb::unnest_var(topics_tier_1) |>
  dplyr::filter(!is.na(topic_tier_1)) |>
  dplyr::group_by(topic_tier_1) |>
  dplyr::summarise(average_turnout = mean(turnout, na.rm = TRUE)) |>
  dplyr::mutate(level = "cantonal")

turnout_topic_combined <- dplyr::bind_rows(turnout_topic_national, turnout_topic_cantonal) |>
  dplyr::mutate(level = factor(level, levels = c("national", "cantonal")))
```

```{r}
# Plot

turnout_topic_plot <- ggplot2::ggplot(data = turnout_topic_combined, 
                                            ggplot2::aes(x = reorder(topic_tier_1,
                                                                     average_turnout), 
                                                         y = average_turnout, 
                                                         fill = factor(level))) +
  ggplot2::geom_bar(stat = "identity", position = ggplot2::position_dodge2(reverse = TRUE),
                    width = 0.7) +
  ggplot2::scale_fill_manual(values = c("#3b528b", "#3ade70")) +
  ggplot2::labs(title = ggplot2::element_blank(),
                x = ggplot2::element_blank(),
                y = ggplot2::element_blank(),
                fill = ggplot2::element_blank()) +
  ggplot2::coord_flip() +
  ggplot2::geom_text(ggplot2::aes(label = rdb.report::percent(average_turnout,
                                                              accuracy = 0.1)), 
                     position = ggplot2::position_dodge2(width = 0.9, reverse = TRUE), 
                     hjust = -0.1, 
                     size = 3) +
  ggplot2::scale_y_continuous(labels = rdb.report::label_percent(accuracy = 1),
                              expand = ggplot2::expansion(mult = c(0, 0.3))) +
  salim::ggplot2_theme() +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-turnout-topic
#| fig-cap: "Turnout in national & cantonal referendums by topic, 1970–2024"
#| fig-column: page
#| fig-height: 4.5

turnout_topic_plot
```

As can be seen in @fig-turnout-topic, the turnout also does not vary significantly between different topics. This indicates that the electorate is not
necessarily more interested in certain subjects. In addition, referendums with different topics are oftentimes held on the same ballot date.

## Turnout with same-day national referendum

```{r}
# Data

## pull vector of dates of national referendums
dates_nat <- rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "national") |>
  rdb::as_ballot_dates() |> 
  dplyr::select(date) |>
  dplyr::pull()

## prepare dataset
turnout_sameday <- rdb.report::dataset(part = "national/CH") |>
  dplyr::filter(level == "subnational") |>
  dplyr::filter(decade >= 1970) |>
  dplyr::mutate(natref_sameday =
                  as.factor(dplyr::case_when(date %in% dates_nat ~ "yes",
                                             !(date %in% dates_nat) ~ "no",
                                             TRUE ~ NA))) |> 
  dplyr::select(turnout, date, natref_sameday) |> 
  dplyr::filter(turnout > 0)
```

```{r}
# Plot

plot_turnout_sameday <-
  ggplot2::ggplot(data = turnout_sameday,
                  ggplot2::aes(x = date,
                               y = turnout,
                               color = natref_sameday)) + 
  ggplot2::geom_point() + 
  ggplot2::geom_smooth(ggplot2::aes(group = natref_sameday,
                                    color = paste("mean", natref_sameday, sep = " ")), 
                       method = stats::loess, 
                       se = FALSE,
                       method.args = list(degree = 0, span = 0.1)) +
  ggplot2::labs(title = ggplot2::element_blank(),
                x = ggplot2::element_blank(),
                y = ggplot2::element_blank(),
                colour = "same-day national referendum") +
  ggplot2::scale_y_continuous(labels = rdb.report::percent) +
  ggplot2::scale_colour_manual(breaks = c("no", "yes"),
                               values = c("no" = "#E69F00", 
                                          "yes" = "#3b528b",
                                          "mean no" = "#edc364", 
                                          "mean yes" = "#7bb9eb")) +
  salim::ggplot2_theme() +
  salim::ggplot2_theme_html()
```

```{r}
#| label: fig-turnout-type-same-day
#| fig-cap: "Turnout in cantonal referendums by same-day national referendum status, 1970–2024"
#| fig-column: page
#| fig-height: 4

plot_turnout_sameday
```

@fig-turnout-type-same-day shows the turnout rate for every cantonal referendum since 1970. When distinguishing between cantonal referendums that took place on
the same day as a national referendum and those that did not, we find a significant difference between the turnout rates. National referendums seem to be a
driving force for participation in cantonal referendums. Our data showcases that the turnout rates in cantonal referendums with same-day national referendums
have been consistently higher since 1970. However, further analyses needs to be carried out in order to determine whether same-day national referendums are the
main explanatory factor behind the higher turnout.
