---
engine: knitr
---

# Preface {.unnumbered}

We are pleased to present the *World of Referendums - `r rdb.report::report_year()` Edition* report. It is the second such report compiled by a team at the
*Centre for Democracy Studies Aarau* (ZDA) at the *University of Zurich*, Switzerland. The report is based on data contained in the unique *Referendum Database*
(RDB). We define referendums as [instances](glossary.qmd#referendum-instance) of "(...) \[a\] popular vote on an issue of policy that is organized by the state
or at least by a state-like entity, such as the authorities of a de facto state" [@Mendez.2016, p. 144]. So defined, the referendum includes both votes on
government proposals as well as citizens' initiatives.

In this iteration of the *World of Referendums* report, our aim is to provide a graphical and descriptive assessment of institutional availability and
referendum practice with a special focus on national and subnational referendums in Switzerland. As a word of caution, we would like to remind the reader that
this is a largely atheoretical data report. The data presented may reveal many interesting patterns and further avenues for future analyses based on theories
and concepts from democracy studies, institutionalism and comparative public policy.

This report has been thoroughly compiled and checked by the authors. Any mistakes that remain are our own. We are aware that the *Referendum Database* may
contain inconsistencies or missing events. This is why we are grateful for your critical
feedback`r ifelse(knitr::is_html_output(), ", either using [Hypothes.is](hypothesis.qmd) annotations or ", "")` via e-mail to
[feedback\@rdb.vote](mailto:feedback@rdb.vote){.email}.

## Introduction to the RDB {#sec-intro}

The *Referendum Database* (RDB), formerly known as the *c2d Referendum Database*, is hosted by the *University of Zurich's* *Centre for Democracy Studies Aarau*
(ZDA), an academic research centre dedicated to the study of democracy in Switzerland and around the world.

The RDB is committed to the documentation of referendum results at the national and partly at the subnational level on a global scale, and in particular at the
cantonal level for Switzerland. As of 2024, the RDB contains information on 3,000 referendums at the national level and 15,000 referendums at the subnational
level in over 200 countries and territories worldwide since 1791. For Switzerland alone, the *Referendum Database* contains data on around 700 national
referendums since 1793, and around 7,000 cantonal referendums since 1970. For each of these referendums, we have recorded the institutional context and
characteristics such as the trigger, the question put forward to the voters, the turnout, the outcome, etc. In total, we collect around eighty data points for
each referendum. The RDB can be accessed [**here**](https://c2d.ch/). Alternatively, the R package [**rdb**](https://rdb.rpkg.dev/) is offered to access the
database's content directly.

## History of the RDB

From 1994 to 2007, the Referendum Database was built up and developed at the *Centre for Research on Direct Democracy* (c2d) at the *Department of
Constitutional Law* of the *University of Geneva*. The centre brought together researchers in law, political science and sociology studying direct democracy as
institutions and political practice. The c2d promoted research on direct democracy from a pluridisciplinary perspective and also provided information, advice,
and counselling on various aspects to public authorities [@Auer.2001]. The Referendum Database was originally funded by the *Swiss National Science Foundation*
(SNSF) project *Dynamique et actualité de la démocratie directe dans un Etat fédéral* [grant no 39348](https://data.snf.ch/grants/grant/39348) at the
*University of Geneva*, directed by professors Andreas Auer and Hanspeter Kriesi. It was further developed with funds from the SNSF project *La démocratie
communale en Suisse: vue générale, institutions et expériences dans les villes 1990–2000* [grant no 59366](https://data.snf.ch/grants/grant/59366), and other
projects.

In fall 2007, the *Centre for Research on Direct Democracy* and the *Referendum Database* were migrated to the *Centre for Democracy Studies Aarau*. Maintenance
and development of the RDB was defined as one of the founding purposes of the ZDA. In the following years, the database was further developed, extended, and
improved; for example by closing gaps in the data on voting results in Swiss cantons or by automating the coding of international voting results. In 2018, the
database was completely redesigned and made available in a new format.

Over the years, the RDB has served as the basis for more than fifty scientific publications on direct democracy in Switzerland and worldwide. To support these
research efforts, the RDB strives to become the most comprehensive empirical collection on referendums worldwide. This is why we continue to improve the
database and add further data, especially from votes at the subnational (state and local) levels. At the same time, we are overhauling the RDB data structure to
better encompass the historical and current legal foundations of referendums. The RDB is to provide accurate, up to date, and easily accessible data for
referendum researchers worldwide.

Concurrently, we valorize the existing data in the form of annual reports and academic publications. This is why we initiated this World of Referendums (WoR)
report series. In addition, we strive to regularly publish cutting edge academic research on referendums around the world.

## Acknowledgements

First and foremost, we would like to pay homage to the founders of the *Referendum Database*, namely professors Andreas Auer, Jean-Daniel Delley and Hanspeter
Kriesi, who all worked at the *Centre for Research on Direct Democracy* (c2d) at the *University of Geneva* at the time. Over the years, many additional people
were involved in the development, maintenance and expansion of the *Referendum Database*.

Collaborators at the *University of Geneva* from 1994 to 2007 were in alphabetical order: Andreas Auer, Antje Beck, Marco Breitenmoser, Michael Bützer,
Jean-Daniel Delley, Frédéric Esposito, Philippe Gerber, Sabine Haenni-Hildbrand, Guita Korvalian, Nicolas Kozuchowski, Reto Kreuzer, Hanspeter Kriesi, Claudio
Mascotto, Jan Prince, Irène Renfer, Frank Schuler, Uwe Serdült, Bénédicte Tornay, Alexander Trechsel, Nicolas von Arx, Valérie Vulliez-Boget, Tobias Zellweger,
and Serge Zogg.

Collaborators at the *Centre for Democracy Studies Aarau* at the *University of Zurich* from 2007 until today in alphabetical order include: Mayowa Alaye,
Corsin Bisaz, Salim Brüggemann, Lukas Christen, Magdalena Despotov, Juri Ezzaini, Norina Frehner, Louis Gebistorf, Micha Germann, Andreas Glaser, Robin Gut,
Joey Jüstrich, Kymani Koelewijn, Daniel Kübler, Beat Kuoni, Irina Lehner, Sarah Lüthold, Fernando Mendez, Beat Müller, Joel Probst, Gabriela Rohner, Liana Sala,
Uwe Serdült, Evren Somer, Gianluca Sorrentino, Anastasyia Souslova, Andrin Walla, Yanina Welp, Jonathan Wheatley, and Jonas Wüthrich.

A special thank you goes to the following persons for their inputs and critical comments on this edition of the *World of Referendums* report: Laurent Bernhard,
Benjamin Böhler, Junmo Cheon, Michaela Fischer, Louis Gebistorf, Andreas Glaser, Gabriel Hofmann, Daniel Kübler, Luka Markić, Joel Probst, and Marine Trichet.

<!-- Restart page numbering with different style -->

\clearpage
\newpage
\pagenumbering{arabic}
